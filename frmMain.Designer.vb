﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.btnGenerate = New System.Windows.Forms.Button()
        Me.txtServer = New System.Windows.Forms.TextBox()
        Me.txtDatabase = New System.Windows.Forms.TextBox()
        Me.txtUser = New System.Windows.Forms.TextBox()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.lblServer = New System.Windows.Forms.Label()
        Me.lblDatabase = New System.Windows.Forms.Label()
        Me.lblUser = New System.Windows.Forms.Label()
        Me.lblPassword = New System.Windows.Forms.Label()
        Me.btnBrowsePath = New System.Windows.Forms.Button()
        Me.txtFilePath = New System.Windows.Forms.TextBox()
        Me.lblFilePath = New System.Windows.Forms.Label()
        Me.dtIni = New System.Windows.Forms.DateTimePicker()
        Me.dtEnd = New System.Windows.Forms.DateTimePicker()
        Me.chkRememberPassword = New System.Windows.Forms.CheckBox()
        Me.pnlTopBorder = New System.Windows.Forms.Panel()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.lblDateIni = New System.Windows.Forms.Label()
        Me.lblDateEnd = New System.Windows.Forms.Label()
        Me.pbxLogo = New System.Windows.Forms.PictureBox()
        Me.pnlTopBorder.SuspendLayout()
        CType(Me.pbxLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnGenerate
        '
        Me.btnGenerate.BackColor = System.Drawing.Color.FromArgb(CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.btnGenerate.FlatAppearance.BorderSize = 0
        Me.btnGenerate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnGenerate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnGenerate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGenerate.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGenerate.ForeColor = System.Drawing.Color.White
        Me.btnGenerate.Location = New System.Drawing.Point(112, 488)
        Me.btnGenerate.Name = "btnGenerate"
        Me.btnGenerate.Size = New System.Drawing.Size(192, 48)
        Me.btnGenerate.TabIndex = 10
        Me.btnGenerate.Text = "GERAR RELATÓRIO"
        Me.btnGenerate.UseVisualStyleBackColor = True
        '
        'txtServer
        '
        Me.txtServer.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtServer.Location = New System.Drawing.Point(176, 174)
        Me.txtServer.Name = "txtServer"
        Me.txtServer.Size = New System.Drawing.Size(208, 32)
        Me.txtServer.TabIndex = 1
        '
        'txtDatabase
        '
        Me.txtDatabase.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDatabase.Location = New System.Drawing.Point(176, 208)
        Me.txtDatabase.Name = "txtDatabase"
        Me.txtDatabase.Size = New System.Drawing.Size(208, 32)
        Me.txtDatabase.TabIndex = 2
        '
        'txtUser
        '
        Me.txtUser.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUser.Location = New System.Drawing.Point(176, 240)
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(208, 32)
        Me.txtUser.TabIndex = 3
        '
        'txtPassword
        '
        Me.txtPassword.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword.Location = New System.Drawing.Point(176, 272)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(208, 32)
        Me.txtPassword.TabIndex = 4
        '
        'lblServer
        '
        Me.lblServer.BackColor = System.Drawing.Color.Transparent
        Me.lblServer.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServer.ForeColor = System.Drawing.Color.White
        Me.lblServer.Location = New System.Drawing.Point(0, 174)
        Me.lblServer.Name = "lblServer"
        Me.lblServer.Size = New System.Drawing.Size(160, 32)
        Me.lblServer.TabIndex = 2
        Me.lblServer.Text = "Servidor:"
        Me.lblServer.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblDatabase
        '
        Me.lblDatabase.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDatabase.ForeColor = System.Drawing.Color.White
        Me.lblDatabase.Location = New System.Drawing.Point(0, 206)
        Me.lblDatabase.Name = "lblDatabase"
        Me.lblDatabase.Size = New System.Drawing.Size(160, 32)
        Me.lblDatabase.TabIndex = 2
        Me.lblDatabase.Text = "Banco de dados:"
        Me.lblDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblUser
        '
        Me.lblUser.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUser.ForeColor = System.Drawing.Color.White
        Me.lblUser.Location = New System.Drawing.Point(0, 238)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(160, 32)
        Me.lblUser.TabIndex = 2
        Me.lblUser.Text = "Usuário:"
        Me.lblUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPassword
        '
        Me.lblPassword.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPassword.ForeColor = System.Drawing.Color.White
        Me.lblPassword.Location = New System.Drawing.Point(0, 270)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(160, 32)
        Me.lblPassword.TabIndex = 2
        Me.lblPassword.Text = "Senha:"
        Me.lblPassword.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnBrowsePath
        '
        Me.btnBrowsePath.BackColor = System.Drawing.Color.FromArgb(CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.btnBrowsePath.FlatAppearance.BorderSize = 0
        Me.btnBrowsePath.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnBrowsePath.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(122, Byte), Integer), CType(CType(204, Byte), Integer))
        Me.btnBrowsePath.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBrowsePath.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBrowsePath.ForeColor = System.Drawing.Color.White
        Me.btnBrowsePath.Location = New System.Drawing.Point(357, 335)
        Me.btnBrowsePath.Name = "btnBrowsePath"
        Me.btnBrowsePath.Size = New System.Drawing.Size(27, 27)
        Me.btnBrowsePath.TabIndex = 7
        Me.btnBrowsePath.Text = "..."
        Me.btnBrowsePath.UseVisualStyleBackColor = True
        '
        'txtFilePath
        '
        Me.txtFilePath.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFilePath.Location = New System.Drawing.Point(32, 368)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.Size = New System.Drawing.Size(352, 32)
        Me.txtFilePath.TabIndex = 6
        '
        'lblFilePath
        '
        Me.lblFilePath.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFilePath.ForeColor = System.Drawing.Color.White
        Me.lblFilePath.Location = New System.Drawing.Point(32, 336)
        Me.lblFilePath.Name = "lblFilePath"
        Me.lblFilePath.Size = New System.Drawing.Size(192, 32)
        Me.lblFilePath.TabIndex = 2
        Me.lblFilePath.Text = "Caminho do arquivo:"
        Me.lblFilePath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtIni
        '
        Me.dtIni.CustomFormat = "dd/MM/yyyy"
        Me.dtIni.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right
        Me.dtIni.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtIni.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtIni.Location = New System.Drawing.Point(32, 432)
        Me.dtIni.Name = "dtIni"
        Me.dtIni.Size = New System.Drawing.Size(160, 32)
        Me.dtIni.TabIndex = 8
        '
        'dtEnd
        '
        Me.dtEnd.CustomFormat = "dd/MM/yyyy"
        Me.dtEnd.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right
        Me.dtEnd.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtEnd.Location = New System.Drawing.Point(224, 432)
        Me.dtEnd.Name = "dtEnd"
        Me.dtEnd.Size = New System.Drawing.Size(160, 32)
        Me.dtEnd.TabIndex = 9
        '
        'chkRememberPassword
        '
        Me.chkRememberPassword.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRememberPassword.ForeColor = System.Drawing.Color.White
        Me.chkRememberPassword.Location = New System.Drawing.Point(176, 304)
        Me.chkRememberPassword.Name = "chkRememberPassword"
        Me.chkRememberPassword.Size = New System.Drawing.Size(192, 27)
        Me.chkRememberPassword.TabIndex = 5
        Me.chkRememberPassword.Text = "Lembrar senha"
        Me.chkRememberPassword.UseVisualStyleBackColor = True
        '
        'pnlTopBorder
        '
        Me.pnlTopBorder.BackColor = System.Drawing.Color.FromArgb(CType(CType(12, Byte), Integer), CType(CType(12, Byte), Integer), CType(CType(12, Byte), Integer))
        Me.pnlTopBorder.Controls.Add(Me.btnClose)
        Me.pnlTopBorder.Controls.Add(Me.lblTitle)
        Me.pnlTopBorder.Cursor = System.Windows.Forms.Cursors.Default
        Me.pnlTopBorder.Location = New System.Drawing.Point(0, 0)
        Me.pnlTopBorder.Margin = New System.Windows.Forms.Padding(0)
        Me.pnlTopBorder.Name = "pnlTopBorder"
        Me.pnlTopBorder.Size = New System.Drawing.Size(416, 32)
        Me.pnlTopBorder.TabIndex = 11
        '
        'btnClose
        '
        Me.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.btnClose.BackColor = System.Drawing.Color.Transparent
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(35, Byte), Integer))
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Image = Global.LarPowerReport.My.Resources.Resources.ic_close
        Me.btnClose.Location = New System.Drawing.Point(371, 0)
        Me.btnClose.Margin = New System.Windows.Forms.Padding(0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.btnClose.Size = New System.Drawing.Size(45, 32)
        Me.btnClose.TabIndex = 0
        Me.btnClose.TabStop = False
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.ForeColor = System.Drawing.Color.White
        Me.lblTitle.Location = New System.Drawing.Point(0, 0)
        Me.lblTitle.Margin = New System.Windows.Forms.Padding(0)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Padding = New System.Windows.Forms.Padding(4, 6, 0, 0)
        Me.lblTitle.Size = New System.Drawing.Size(114, 39)
        Me.lblTitle.TabIndex = 2
        Me.lblTitle.Text = "Relatório"
        Me.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDateIni
        '
        Me.lblDateIni.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateIni.ForeColor = System.Drawing.Color.White
        Me.lblDateIni.Location = New System.Drawing.Point(32, 400)
        Me.lblDateIni.Name = "lblDateIni"
        Me.lblDateIni.Size = New System.Drawing.Size(152, 32)
        Me.lblDateIni.TabIndex = 2
        Me.lblDateIni.Text = "Data inicial:"
        Me.lblDateIni.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDateEnd
        '
        Me.lblDateEnd.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateEnd.ForeColor = System.Drawing.Color.White
        Me.lblDateEnd.Location = New System.Drawing.Point(220, 400)
        Me.lblDateEnd.Name = "lblDateEnd"
        Me.lblDateEnd.Size = New System.Drawing.Size(152, 32)
        Me.lblDateEnd.TabIndex = 2
        Me.lblDateEnd.Text = "Data final:"
        Me.lblDateEnd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pbxLogo
        '
        Me.pbxLogo.Image = Global.LarPowerReport.My.Resources.Resources.logo
        Me.pbxLogo.Location = New System.Drawing.Point(144, 32)
        Me.pbxLogo.Name = "pbxLogo"
        Me.pbxLogo.Size = New System.Drawing.Size(128, 128)
        Me.pbxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbxLogo.TabIndex = 12
        Me.pbxLogo.TabStop = False
        '
        'frmMain
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(416, 568)
        Me.Controls.Add(Me.pnlTopBorder)
        Me.Controls.Add(Me.pbxLogo)
        Me.Controls.Add(Me.chkRememberPassword)
        Me.Controls.Add(Me.dtEnd)
        Me.Controls.Add(Me.dtIni)
        Me.Controls.Add(Me.btnBrowsePath)
        Me.Controls.Add(Me.lblDateEnd)
        Me.Controls.Add(Me.lblDateIni)
        Me.Controls.Add(Me.lblFilePath)
        Me.Controls.Add(Me.lblPassword)
        Me.Controls.Add(Me.lblUser)
        Me.Controls.Add(Me.lblDatabase)
        Me.Controls.Add(Me.lblServer)
        Me.Controls.Add(Me.txtFilePath)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.txtUser)
        Me.Controls.Add(Me.txtDatabase)
        Me.Controls.Add(Me.txtServer)
        Me.Controls.Add(Me.btnGenerate)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Relatório"
        Me.pnlTopBorder.ResumeLayout(False)
        Me.pnlTopBorder.PerformLayout()
        CType(Me.pbxLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnGenerate As Button
    Friend WithEvents txtServer As TextBox
    Friend WithEvents txtDatabase As TextBox
    Friend WithEvents txtUser As TextBox
    Friend WithEvents txtPassword As TextBox
    Friend WithEvents lblServer As Label
    Friend WithEvents lblDatabase As Label
    Friend WithEvents lblUser As Label
    Friend WithEvents lblPassword As Label
    Friend WithEvents btnBrowsePath As Button
    Friend WithEvents txtFilePath As TextBox
    Friend WithEvents lblFilePath As Label
    Friend WithEvents dtIni As DateTimePicker
    Friend WithEvents dtEnd As DateTimePicker
    Friend WithEvents chkRememberPassword As CheckBox
    Friend WithEvents pnlTopBorder As Panel
    Friend WithEvents btnClose As Button
    Friend WithEvents lblTitle As Label
    Friend WithEvents lblDateIni As Label
    Friend WithEvents lblDateEnd As Label
    Friend WithEvents pbxLogo As PictureBox
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents SobreToolStripMenuItem As ToolStripMenuItem
End Class
