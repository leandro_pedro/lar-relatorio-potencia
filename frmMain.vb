﻿Imports Microsoft.Win32

Public Class frmMain
    Private Const encryptingKey As String = "3-qLg_8a"

    Private newPoint As System.Drawing.Point
    Private posX As Integer
    Private posY As Integer

    ' Arrastar form atraves do panel
    Private Sub pnlTopBorder_MouseDown(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles pnlTopBorder.MouseDown
        posX = Control.MousePosition.X - Me.Location.X
        posY = Control.MousePosition.Y - Me.Location.Y
    End Sub

    Private Sub pnlTopBorder_MouseMove(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles pnlTopBorder.MouseMove
        If e.Button = Windows.Forms.MouseButtons.Left Then
            newPoint = Control.MousePosition
            newPoint.X -= (posX)
            newPoint.Y -= (posY)
            Me.Location = newPoint
        End If
    End Sub

    Private Sub FrmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim strServer As String
        Dim strDb As String
        Dim strUser As String
        Dim strPwd As String
        Dim strFilePath As String
        Dim bRememberPassword As Boolean
        Dim wrapper As New Simple3Des(encryptingKey)
        Dim key As RegistryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\Lar", False)

        If key Is Nothing Then
            key = Registry.CurrentUser.CreateSubKey("SOFTWARE\Lar")
        End If

        strServer = key.GetValue("Server", "DESKTOP-QSEFPRJ\SQLExpress01")
        strDb = key.GetValue("Database", "Lar")
        strUser = key.GetValue("User", "sa")
        strPwd = key.GetValue("Password", "")
        bRememberPassword = key.GetValue("RememberPassword", False)
        strFilePath = key.GetValue("FilePath", My.Computer.FileSystem.CurrentDirectory & "\MyFile.csv")

        ' Descriptografa senha
        If strPwd <> "" Then
            strPwd = wrapper.DecryptData(strPwd)
        End If

        ' Valores padrao caso registro nao exista.
        txtServer.Text = strServer
        txtDatabase.Text = strDb
        txtUser.Text = strUser
        txtPassword.Text = strPwd
        txtFilePath.Text = strFilePath
        chkRememberPassword.Checked = bRememberPassword

        ' Data inicial/final.
        dtIni.Value = DateTime.Now.AddMonths(-3)
        dtEnd.Value = DateTime.Now.AddDays(-1)

        ' Add menu de contexto no titulo do form (lblTitle)
        Dim menu As New ContextMenuStrip()
        Dim about As New ToolStripMenuItem()

        lblTitle.ContextMenuStrip = menu

        about.Name = "mnuAbout"
        about.Text = "&Sobre..."
        AddHandler about.Click, AddressOf about_Click
        menu.Items.Add(about)
    End Sub

    Private Sub BtnGenerate_Click(sender As Object, e As EventArgs) Handles btnGenerate.Click
        btnGenerate.Enabled = False
        If GenerateReport() Then
            MsgBox("Arquivo gerado com sucesso!", vbInformation, "Relatório")
        End If
        btnGenerate.Enabled = True
    End Sub

    Private Sub btnGenerate_EnabledChanged(sender As Object, e As EventArgs) Handles btnGenerate.EnabledChanged
        Dim btn As Button
        btn = sender

        btn.Text = IIf(btn.Enabled, "GERAR RELATÓRIO", "GERANDO RELATÓRIO...")
    End Sub

    Private Sub BtnBrowsePath_Click(sender As Object, e As EventArgs) Handles btnBrowsePath.Click
        Dim sf As SaveFileDialog = New SaveFileDialog()
        sf.Filter = "Arquivo csv (*.csv)|*.csv| Arquivo texto (*.txt)|*.txt"
        sf.AddExtension = True
        sf.OverwritePrompt = False

        If sf.ShowDialog() Then
            txtFilePath.Text = sf.FileName
        End If
    End Sub

    Private Function GenerateReport() As Boolean
        Dim cnt As Object = CreateObject("ADODB.Connection")
        Dim rst As Object = CreateObject("ADODB.Recordset")

        Dim outFile As IO.StreamWriter
        Dim recArray As Array
        Dim fldCount As Integer
        Dim recCount As Long
        Dim iCol As Integer
        Dim iRow As Integer

        Dim strServer As String = Trim(txtServer.Text)
        Dim strDb As String = Trim(txtDatabase.Text)
        Dim strUser As String = Trim(txtUser.Text)
        Dim strPwd As String = Trim(txtPassword.Text)
        Dim strFilePath = Trim(txtFilePath.Text)

        Dim strQuery As String

        If Not FileNameIsOk(strFilePath) Then
            Return False
        End If

        strQuery =
                " USE Lar" &
                " SELECT" &
                "   CONVERT(VARCHAR, DATEADD(day, -2, d.[Date]), 103) [DIA]" &
                "   ,CONVERT(VARCHAR, d.[Time], 108) [HORA]" &
                "   ,STR(d.[Max], 10, 2) + ' kW' [POTÊNCIA MÁXIMA]" &
                " FROM (" &
                "   SELECT" &
                "       c.[Date]" &
                "       ,MAX(c.[Time]) [Time]" &
                "       ,c.[Max]" &
                "   FROM (  " &
                "       SELECT" &
                "               b.[Date]" &
                "               ,b.[Max]" &
                "               ,CAST(CAST(a.E3TimeStamp AS DATETIME) AS TIME) [Time]" &
                "       FROM (" &
                "           SELECT" &
                "                   CAST(CAST(E3TimeStamp AS DATETIME) AS DATE) [Date]" &
                "                   ,MAX(FieldValue) [Max]" &
                "           FROM HistoricoMedicoes" &
                "           WHERE FieldID = (SELECT FieldID FROM HistoricoMedicoes_Fields WHERE FieldName LIKE '%LAR_138_TR1_51H1_POTENCIA_ATIVA_138%')" &
                "           GROUP BY CAST(CAST(E3TimeStamp AS DATETIME) AS DATE), FieldID" &
                "       ) b, HistoricoMedicoes a" &
                "       WHERE b.Max = a.FieldValue AND b.Date = CAST(CAST(a.E3TimeStamp AS DATETIME) AS DATE)" &
                "   ) c" &
                "   GROUP BY c.Date, c.Max" &
                " ) d" &
                " WHERE d.[Date] BETWEEN '" & dtIni.Value.AddDays(2).ToString("yyyy-MM-dd") & "' AND '" & dtEnd.Value.AddDays(2).ToString("yyyy-MM-dd") & "'" &
                " ORDER BY d.[Date] DESC"

        ' Open connection to the database
        Try
            cnt.Open("Driver={SQL Server};Server=" & strServer & ";Database=" & strDb &
                    ";Uid=" & strUser & ";Pwd=" & strPwd & ";")
            cnt.CommandTimeout = 180
        Catch ex As Exception
            MsgBox(ex.Message, vbExclamation, "")
            Return False
        End Try

        ' Open recordset based on Orders table
        Try
            rst.Open(strQuery, cnt)
        Catch ex As Exception
            MsgBox("Falha ao consultar banco de dados." & vbNewLine & vbNewLine & ex.Message, vbExclamation, "")
            Return False
        End Try

        If rst.BOF And rst.EOF Then
            MsgBox("Nenhum dados encontrado para o período informado.", vbInformation, "Atenção")
            Return False
        End If

        Try
            outFile = My.Computer.FileSystem.OpenTextFileWriter(strFilePath, False)
        Catch ex As Exception
            MsgBox(ex.Message, vbExclamation, "Atenção")
            Return False
        End Try

        recArray = rst.GetRows

        ' Copia nome dos campos de consulta pra cabecalho do csv
        fldCount = rst.Fields.Count
        For iCol = 1 To fldCount
            outFile.Write(rst.Fields(iCol - 1).Name & ";")
        Next

        recCount = UBound(recArray, 2) + 1
        Console.WriteLine(recCount)
        outFile.WriteLine("")

        For iRow = 0 To recCount - 1
            For iCol = 0 To fldCount - 1
                outFile.Write(recArray(iCol, iRow) & ";")
            Next iCol 'next record
            outFile.WriteLine("")
        Next iRow 'next field

        outFile.Close()

        ' Salva valores no registro
        Dim key As RegistryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\Lar", True)
        If key Is Nothing Then
            key = Registry.CurrentUser.CreateSubKey("SOFTWARE\Lar")
        End If
        key.SetValue("Server", txtServer.Text)
        key.SetValue("Database", txtDatabase.Text)
        key.SetValue("User", txtUser.Text)
        key.SetValue("FilePath", txtFilePath.Text)
        key.SetValue("RememberPassword", chkRememberPassword.Checked)

        If chkRememberPassword.Checked Then
            Dim wrapper As New Simple3Des(encryptingKey)
            strPwd = wrapper.EncryptData(txtPassword.Text)
        Else
            strPwd = ""
        End If
        key.SetValue("Password", strPwd)

        Return True
    End Function

    Private Function FileNameIsOk(ByVal fileName As String) As Boolean
        Dim file As String
        Dim directory As String
        Dim extension As String
        Dim result As Boolean

        If fileName = "" Then
            Return False
        End If

        file = IO.Path.GetFileName(fileName)
        directory = IO.Path.GetDirectoryName(fileName)
        extension = IO.Path.GetExtension(file)

        If IO.File.Exists(fileName) Then
            If MsgBox("'" & fileName & "' já existe." & vbNewLine &
                      "Deseja substituí-lo?", vbExclamation + vbYesNo, "Salvar como") = vbNo Then
                Return False
            End If
        End If

        Dim dir As New System.IO.DirectoryInfo(directory)

        result = (Not file.Intersect(IO.Path.GetInvalidFileNameChars()).Any() _
                         And {".csv", ".txt"}.Contains(extension) _
                         And dir.Exists)
        If Not result Then
            MsgBox("Por favor, escolha um nome válido para o arquivo.", vbExclamation, "Atenção")
        End If

        Return result
    End Function

    Private Sub BtnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub about_Click(sender As Object, e As EventArgs)
        frmAbout.Show()
    End Sub
End Class